#include <Arduino.h>

#include <PubSubClient.h>

#include <SPI.h>
#include <Dhcp.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <Dns.h>

#define alarmPTSPin 6 // Pin to set alarm - connect to relay
#define alarmINTPin 2
#define alarmSETPin 3
#define alarmCONFPin 4
#define alarmABORTPin 5
const char* HASS_STATUS_TOPIC = "hass/status";

byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};
EthernetClient ethClient;
PubSubClient mqtt_client(ethClient);
char server[] = "192.168.1.205";
String currentStatus;

// Alarm signals
int ALARM_SET;
int ALARM_INT;
int ALARM_ABORT;
int ALARM_CONF;

void alarm_on()
{
  digitalWrite(alarmPTSPin, LOW);
}

void alarm_off()
{
  // If the alarm has been set via the keypad then we need to
  // catch up by closing relay before opening again
  if (ALARM_SET)
  {
    alarm_on();
    delay(1000);
  }
  digitalWrite(alarmPTSPin, HIGH);
}

void check_alarm_status(int set_status, int int_status, int conf_status, int abort_status)
{
  if (ALARM_SET != set_status)
  {
    ALARM_SET = set_status;
    if (ALARM_SET == LOW)
    {
      Serial.println("Alarm SET");
      Serial.println(mqtt_client.publish("home/alarm", "armed_away"));
      currentStatus = "armed_away";
    }
    else
    {
      Serial.println("Alarm UNSET");
      Serial.println(mqtt_client.publish("home/alarm", "disarmed"));
      currentStatus = "disarmed";
    }
  }

  if (ALARM_INT != int_status)
  {
    ALARM_INT = int_status;
    if (ALARM_INT == LOW)
    {
      Serial.println("Alarm triggered!");
      Serial.println(mqtt_client.publish("home/alarm", "triggered"));
      currentStatus = "triggered";
    }
  }

  if (ALARM_CONF != conf_status)
  {
    ALARM_CONF = int_status;
    if (ALARM_CONF == LOW)
    {
      Serial.println("Alarm confirmed triggered!");
      Serial.println(mqtt_client.publish("home/alarm", "triggered"));
      currentStatus = "triggered";
    }
  }
}

void mqtt_callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  char cmd[length + 1];
  for (unsigned int i = 0; i < length; i++)
  {
    cmd[i] = payload[i];
  }
  cmd[length] = 0;

  String command = String(cmd);
  Serial.println(command);

  if (topic == HASS_STATUS_TOPIC) {
    // Status message from Homeassistant
    if (command == "online") {
      int strLen = currentStatus.length() + 1;
      char statusBuffer[strLen];
      currentStatus.toCharArray(statusBuffer, strLen);
      mqtt_client.publish("home/alarm", statusBuffer);
    }
  } else {

    if (command == "ARM_AWAY")
    {
      mqtt_client.publish("home/alarm", "pending");
      alarm_on();
    }

    if (command == "DISARM")
    {
      alarm_off();
    }
  }
  //  for (int i=0;i<length;i++) {
  //    Serial.print((char)payload[i]);
  //*((byte *) (&mqtt_message) + i) = payload[i];
  //  }
  Serial.println();
}

void mqtt_reconnect()
{
  // Loop until we're reconnected
  while (!mqtt_client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqtt_client.connect("arduinoClient", "robin", "robpass"))
    {
      Serial.println("MQTT connected");
      // Once connected, publish an announcement...
      if (ALARM_SET == LOW)
      {
        mqtt_client.publish("home/alarm", "armed_away");
      }
      else
      {
        mqtt_client.publish("home/alarm", "disarmed");
      }
      // ... and resubscribe
      mqtt_client.subscribe("home/alarm/set");
      mqtt_client.subscribe(HASS_STATUS_TOPIC);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqtt_client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// Arduino bits

void setup()
{
  pinMode(alarmPTSPin, OUTPUT);
  pinMode(alarmINTPin, INPUT);
  pinMode(alarmCONFPin, INPUT);
  pinMode(alarmABORTPin, INPUT);
  // Get initial state
  ALARM_INT = digitalRead(alarmINTPin);
  ALARM_CONF = digitalRead(alarmCONFPin);
  // ALARM_ABORT - digitalRead(alarmABORTPin);
  ALARM_SET = digitalRead(alarmSETPin);
  if (ALARM_SET == LOW)
  {
    alarm_on();
  }
  else
  {
    alarm_off();
  }

  Serial.begin(57600);
  Serial.println("Starting network...");
  // Configure MQTT
  Serial.print("Connect to MQTT on ");
  Serial.println(server);
  mqtt_client.setServer(server, 1883);
  mqtt_client.setCallback(mqtt_callback);

  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    while (true)
      ;
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);
}

void loop()
{
  if (!mqtt_client.connected())
  {
    mqtt_reconnect();
  }
  mqtt_client.loop();
  //ALARM_SET = digitalRead(alarmSETPin);
  //ALARM_INT = digitalRead(alarmINTPin);
  check_alarm_status(digitalRead(alarmSETPin), digitalRead(alarmINTPin), digitalRead(alarmCONFPin), digitalRead(alarmABORTPin));
}
